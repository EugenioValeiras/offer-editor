import alt from '../alt'
import OffersActions from '../actions/OffersActions';

class OfferStore {
  constructor() {
    this.loading = false;
    this.loadingPlans = false;
    this.loadingOffers = false;
    this.loadingPeriod = false;
    this.loadingPeople = false;

    this.periodsList = [];
    this.offers = [];
    this.plans = [];
    this.plansV2 = []; //Plans related to one plan period
    this.planPeriod = {from: {}, to:{}};
    this.employers = [];
    this.typesOptions = [];
    this.vendors = [];
    this.templates = [];
    this.rates = [];
    this.people = {};

    this.counts = {
      TotalCensus: 0,
      Eligible: 0,
      Excluded: 0,
      SSOff: 0,
      SSPending: 0,
      SSOn: 0,
      StatusUPI: 0,
      StatusElection: 0,
      StatusEnrollment: 0,
      StatusDone: 0,
      Waived: 0,
      HaveForms: 0,
      FormsReady: 0,
      FormsPosted: 0
    };

    this.bindListeners({
      handleFetchPeriods: OffersActions.FETCH_PERIODS,
      handleUpdatePeriods: OffersActions.UPDATE_PERIODS,
      handleFetchOffers: OffersActions.FETCH_OFFERS,
      handleUpdateOffers: OffersActions.UPDATE_OFFERS,
      handleUpdatePlans: OffersActions.UPDATE_PLANS,
      handleFetchPlansV2: OffersActions.FETCH_PLANS_V2,
      handleUpdatePlansV2: OffersActions.UPDATE_PLANS_V2,
      handleUpdateEmployers: OffersActions.UPDATE_EMPLOYERS,
      handleUpdateTypesOptions: OffersActions.UPDATE_TYPES,
      handleUpdateVendors: OffersActions.UPDATE_VENDORS,
      handleFetchPeriod: OffersActions.FETCH_PERIOD,
      handleUpdatePeriod: OffersActions.UPDATE_PERIOD,
      handleUpdateTemplates: OffersActions.UPDATE_TEMPLATES,
      handleUpdateRates: OffersActions.UPDATE_RATES,
      handleFetchPeople: OffersActions.FETCH_PEOPLE,
      handleUpdatePeople: OffersActions.UPDATE_PEOPLE,
      handleUpdateCount: OffersActions.UPDATE_COUNTS,
      handleUpdatePerson: OffersActions.UPDATE_PERSON
    });
  }

  handleUpdateCount(counts) {
    this.counts = counts;
  }

  handleFetchPeriods() {
    this.loading = true;
  }

  handleUpdatePeriods(periods) {
    this.loading = false;
    this.periodsList = periods;
  }

  handleFetchOffers() {
    this.loadingOffers = true;
  }

  handleUpdateOffers(offers) {
    this.loadingOffers = false;
    this.offers = offers;
  }

  handleUpdateTemplates(templates) {
    this.templates = templates;
  }

  handleUpdateEmployers(employers) {
    this.employers = employers;
  }

  handleUpdateTypesOptions(typesOptions) {
    this.typesOptions = typesOptions;
  }

  handleUpdateVendors(vendors) {
    this.vendors = vendors;
  }

  handleUpdateRates(rates) {
    this.rates = rates;
  }

  handleUpdatePlans(plans) {
    this.plans = plans;
  }

  handleFetchPlansV2() {
    this.loadingPlans = true;
  }

  handleUpdatePlansV2(plans) {
    this.loadingPlans = false;
    this.plansV2 = plans;
  }

  handleFetchPeriod() {
    this.loadingPeriod = true;
  }

  handleUpdatePeriod(planPeriod) {
    this.loadingPeriod = false;
    this.planPeriod = planPeriod;
  }

  handleUpdatePerson(person) {
    let found = _.find(this.people, person);
    found = person;
  }

  handleFetchPeople() {
    this.loadingPeople = true;
  }

  handleUpdatePeople(people) {
    this.loadingPeople = false;
    this.people = people;
  }
}

module.exports = alt.createStore(OfferStore, 'OfferStore');
