import React from 'react';
import Button from '../../node_modules/react-bootstrap/lib/Button';
import OverlayTrigger from '../../node_modules/react-bootstrap/lib/OverlayTrigger';
import Select from '../../lib/react-select/Select';
import PopOver from './PopOver.jsx';

const TYPES = {
  PERCENT: 'percent',
  FIXED: 'fixed'
};

const DEFAULT_OPTIONS = [
  { value: 'forEmployee', label: 'For Employee', original: true },
  { value: 'forSpouse', label: 'For Spouse', original: true },
  { value: 'forChildren', label: 'For Children', original: true}
];

class ContributionToBenefit extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      contribution: null,
      editingValue: null,
      typeSelect: TYPES.PERCENT,
      options:  _.clone(DEFAULT_OPTIONS)
    }
  }

  componentWillMount() {
    this.setState({contribution: this.props.entity.contribution});
  }

  componentWillReceiveProps(nextProps) {
    this.setState( {
      contribution: nextProps.entity.contribution || null,
      editingValue: null,
      typeSelect: TYPES.PERCENT,
      options:  _.clone(DEFAULT_OPTIONS)
    });
  }

  dataChanged(data) {
    this.setState(data);
  }

  editValueClick(e, option) {
    var self = this;
    this.setState({editingValue: option.value, contributionToEdit: option.relatedData}, function() {
      self.refs.contributionPopoverTrigger.show();
    });

  }

  logChange(val, selected) {
    let newValues = _.where(selected, {original: true});
    if (newValues.length > 0) {
      //reset state
      this.setState({editingValue: null, newSelection: selected, contributionToEdit: null});
      this.refs.contributionPopoverTrigger.show();
    } else {
      let newOptions = _.union(selected, DEFAULT_OPTIONS);
      this._updateEntity(selected);
      this.setState({selected: selected, options: newOptions});
    }
  }

  _buildLabel(optionValue, contribution) {
    let label = '';
    if (optionValue) {
      label = _.findWhere(DEFAULT_OPTIONS, {value: optionValue});
      label = label.label + ' ';
    }

    if (contribution.type === TYPES.FIXED) {
      label += 'fixed amount of $' + contribution.amount + ' per month';
    } else {
      label += contribution.amount + '% of the premium';

      if (contribution.noMoreAmount) {
          label += ' no more than $' + contribution.noMoreAmount;
      }
    }

    return label;
  }

  _save(contribution) {
    this._saveOptions(contribution);
    this.refs.contributionPopoverTrigger.hide();
  }

  _removeSelected() {
    this.setState({selected: null});
    this._updateEntity(null);
  }

  _saveOptions(contribution) {
    //Get new selected
    let selectedValues;
    let newOptions = this.state.options;

    if (!this.state.editingValue) {
      selectedValues = this.state.newSelection;
      let newSelected = _.findWhere(selectedValues, {original: true});
      selectedValues = _.without(selectedValues, newSelected);

      newSelected = _.clone(newSelected);
      newSelected.label = this._buildLabel(newSelected.value,contribution );
      newSelected.original = false;
      newSelected.relatedData = contribution;

      newOptions.push(newSelected); //Add as available option
      selectedValues.push(newSelected); //Add new options to selected values
    } else {
      selectedValues = this.state.contribution;
      let selectedValue = _.findWhere(selectedValues, {value: this.state.editingValue});
      selectedValue.label = this._buildLabel(this.state.editingValue, contribution);
      selectedValue.relatedData = contribution;
    }

    this.setState({selected: selectedValues, options: newOptions});

    this._updateEntity(selectedValues);
  }

  _updateEntity(selectedValues) {
    let entity = this.props.entity;
    entity.contribution = selectedValues;
    this.props.onUpdate(entity);
  }

  closePopover() {
    this.refs.contributionPopoverTrigger.hide();
  }

  _buildButton() {
    let isEmpty = !this.state.contribution || this.state.contribution.length == 0;
    return (<Select placeholder="" onItemClick={this.editValueClick.bind(this)} multi searchable={false}
                    className={isEmpty ? 'empty-contribution' : null}
                    clearable={false} name="form-field-name" value={this.state.contribution} options={this.state.options}
                    onChange={this.logChange.bind(this)} />);
  }


  render() {
    return (
      <div className="contribution-selects">
        {this.props.disable ?
          <button type="button" className="btn btn-link disabled">+ Add Contribution to Benefits</button>  :
          <OverlayTrigger ref="contributionPopoverTrigger" disabled trigger="click" placement="top" rootClose
                          overlay={<PopOver {...this.props} onClose={this.closePopover.bind(this)} title="Add Contribution to Benefit"
                          contribution={this.state.contributionToEdit} onUpdate={this._save.bind(this)} />}>
            {this._buildButton()}
          </OverlayTrigger>}
      </div>
    );
  }
}


ContributionToBenefit.propTypes = {
  disable: React.PropTypes.bool,
  entity: React.PropTypes.object,
  onUpdate: React.PropTypes.func
};

module.exports = ContributionToBenefit;
