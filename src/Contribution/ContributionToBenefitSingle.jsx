import React from 'react';
import Button from '../../node_modules/react-bootstrap/lib/Button';
import OverlayTrigger from '../../node_modules/react-bootstrap/lib/OverlayTrigger';
import PopOver from './PopOver.jsx';

const TYPES = {
  PERCENT: 'percent',
  FIXED: 'fixed'
};

class ContributionToBenefitSingle extends React.Component{
  constructor(props) {
    super(props);
    this.state = {contribution: null}
  }

  componentWillMount() {
    this.setState({contribution: this.props.entity.contribution});
  }

  componentWillReceiveProps(nextProps) {
    this.setState( {contribution: nextProps.entity.contribution});
  }

  _buildLabel() {
    let label = '';

    let contribution = this.props.entity.contribution;

    if (contribution.type === TYPES.FIXED) {
      label += 'fixed amount of $' + contribution.amount + ' per month';
    } else {
      label += contribution.amount + '% of the premium';

      if (contribution.noMoreAmount) {
          label += ' no more than $' + contribution.noMoreAmount;
      }
    }

    return label;
  }

  _removeSelected() {
    this.setState({contribution: null});
    this._updateEntity(null);
  }

  _updateEntity(selectedValues) {
    let entity = this.props.entity;
    entity.contribution = selectedValues;
    this.props.onUpdate(entity);
  }


  closePopover() {
    this.refs.contributionPopoverTrigger.hide();
  }

  _buildButton() {
    if (this.state.contribution) {
      return (
        <label className="selected-waiting-period">
          <span>{this._buildLabel()}</span>
          <span className="selection__choice__remove" onClick={this._removeSelected.bind(this)} role="presentation">×</span>
        </label>);
    } else {
      return (<Button bsStyle="link">+ Add Contribution to Benefits</Button>);
    }
  }

  render() {
    return (
      <div className="contribution-selects">
        {this.props.disable ?
          <button type="button" className="btn btn-link disabled">+ Add Contribution to Benefits</button>  :
          <OverlayTrigger ref="contributionPopoverTrigger" disabled trigger="click" placement="top" rootClose
                          overlay={<PopOver {...this.props} onClose={this.closePopover.bind(this)} title="Add Contribution to Benefit"
                          contribution={this.state.contribution} onUpdate={this._updateEntity.bind(this)} />}>
            {this._buildButton()}
          </OverlayTrigger>}
      </div>
    );
  }
}


ContributionToBenefitSingle.propTypes = {
  disable: React.PropTypes.bool,
  entity: React.PropTypes.object,
  onUpdate: React.PropTypes.func
};

module.exports = ContributionToBenefitSingle;
