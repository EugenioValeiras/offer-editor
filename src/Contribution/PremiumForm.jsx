import React from 'react';
import InlineEdit from 'react-edit-inline';

class PremiumForm extends React.Component{
  constructor(props) {
    super(props);
    this.state = {noMore: false};
  }

  componentWillMount() {
    this.setState({noMore: !!this.props.noMoreAmount});
  }

  componentWillReceiveProps(nextProps) {
    this.setState({noMore: !!nextProps.noMoreAmount});
  };

  _onChangeHandler() {
    let newValue = !this.state.noMore;
    if (!newValue) {
      this.props.onUpdate({noMoreAmount: null});
    }
    this.setState({noMore: newValue});
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-offset-1 col-md-3">
          <InlineEdit className="editable-click" activeClassName="name-input-short" text={this.props.amount ? this.props.amount : '0' } paramName="amount" change={this.props.onUpdate}   />
          &nbsp; %
        </div>
        <div className="col-md-4">
          <label>
            <input type="checkbox" checked={this.state.noMore}  onChange={this._onChangeHandler.bind(this)} name="noMoreThan" value="true" />
            But no more than
          </label>
        </div>
        {this.state.noMore ?
          <div className="col-md-3">
            $ <InlineEdit className="editable-click" activeClassName="name-input-short" text={this.props.noMoreAmount ? this.props.noMoreAmount : '0'} paramName="noMoreAmount" change={this.props.onUpdate}   />
          </div> : null}
      </div>
    );
  }
}


PremiumForm.propTypes = {
  amount: React.PropTypes.string,
  noMoreAmount: React.PropTypes.string,
  onUpdate: React.PropTypes.func
};

module.exports = PremiumForm;
