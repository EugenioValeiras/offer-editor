import React from 'react';

class YearSelect extends React.Component{
  constructor(props) {
    super(props);
    this.state = {};
  }

  change(e) {
    this.props.onChange(e.currentTarget.value);
  }

  render() {
    return (
      <select className="form-control"
              onChange={this.change.bind(this)}
              value={this.props.value || '' } >

        <option value="">Pick Year</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
        <option value="2019">2019</option>
        <option value="2020">2020</option>
        <option value="2021">2021</option>
        <option value="2022">2022</option>
        <option value="2023">2023</option>
        <option value="2024">2024</option>
        <option value="2025">2025</option>
        <option value="2026">2026</option>
        <option value="2027">2027</option>
        <option value="2028">2028</option>
        <option value="2029">2029</option>
        <option value="2030">2030</option>
      </select>
    );
  }
}

YearSelect.propTypes = {
  onChange: React.PropTypes.func,
  value: React.PropTypes.string
};

module.exports = YearSelect;
