import React from 'react';

class MonthSelect extends React.Component{
  constructor(props) {
    super(props);
    this.state = {};
  }

  change(e) {
    this.props.onChange(e.currentTarget.value);
  }

  render() {
    return (
      <select className="form-control"
              onChange={this.change.bind(this)}
              value={this.props.value || '' } >
        <option value="">Pick Month</option>
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
    );
  }
}

MonthSelect.propTypes = {
  onChange: React.PropTypes.func,
  value: React.PropTypes.string
};

module.exports = MonthSelect;
