export function formatDatesPeriod(from, to) {
  let fromMoment = moment(from);
  let toMoment = moment(to);

  return fromMoment.format('DD-MMM-YYYY') + ' to ' + toMoment.format('DD-MMM-YYYY') ;
}

export function monthYearToDateMoment(month, year) {
  let dateString = month + '-' + year;
  let momentDate = moment(dateString, 'MM-YYYY');

  return momentDate.isValid() ? momentDate : null;
}
