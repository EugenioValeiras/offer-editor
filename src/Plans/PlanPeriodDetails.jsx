import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import OfferStore from '../stores/OfferStore';
import OffersActions from '../actions/OffersActions';
import OfferEditor from '../OfferEditor.jsx';
import PlanEditor from './PlanEditor.jsx';
import _ from 'lodash';

class PlanPeriodDetails extends React.Component{
  constructor(props) {
    super(props);
    this.onStoreChange = this.onStoreChange.bind(this);
    this.state = OfferStore.getState(); //Offers and Plans
  }

  onStoreChange(state) {
    if (!state.loadingPlans && !state.loadingOffers) {
      this.unBlockUI();
    }
    this.setState(state);
  }

  componentDidMount() {
    this.blockUI();
    OfferStore.listen(this.onStoreChange);
    OffersActions.fetchPlansV2(this.props.params.periodId);
    OffersActions.fetchPlans();
    OffersActions.fetchEmployers();
    OffersActions.fetchTypes();
    OffersActions.fetchVendors();
    OffersActions.fetchRates();
    OffersActions.fetchOffers(this.props.params.periodId);
    OffersActions.fetchPeriod(this.props.params.periodId);
  }

  componentWillUnmount() {
    OfferStore.unlisten(this.onStoreChange);
  }

  add() {
    OffersActions.addOffer(this.state.offers, {name: null, default: false, order: 1, periodId: this.props.params.periodId });
  }

  remove(offer) {
    OffersActions.removeOffer(this.state.offers, offer);
  }

  updateOffer(offer) {
    OffersActions.updateOffer(offer);
  }

  sort(offer, newOrder) {
    OffersActions.syncOrder(this.state.offers, offer, newOrder);
  }

  updatePeriod(period) {
    OffersActions.changePeriod(period);
  }

  addPlanV2(plan) {
    plan.periodId = this.state.planPeriod.objectId;
    OffersActions.addPlanV2(plan);
  }

  updatePlanV2(plan) {
    OffersActions.editPlanV2(plan);
  }

  savePlanV3(plan) {
    plan.objectId ? this.updatePlanV2(plan) : this.addPlanV2(plan);
  }

  searchTemplates(query) {
    OffersActions.fetchTemplates(query);
  }

  blockUI() {
    $(this.refs.mainDiv).block({overlayCSS: {opacity: 0.3}, message: null });
  }

  unBlockUI() {
    $(this.refs.mainDiv).unblock();
  }

  render() {
    /*let planTypes = {
        MEDICAL: 'medical',
        DENTAL: 'dental',
        VISION: 'vision',
        LIFE: 'life',
        test: 'test'
    }; */

    let planTypes = {};

    this.state.plansV2.forEach((plan) => {
      planTypes[_.snakeCase(plan.benefit_type)] = plan.benefit_type;
    });

    return (
      <div ref="mainDiv">
          <PlanEditor planPeriod={this.state.planPeriod} plansList={this.state.plansV2} hideList={false}
                      templates={this.state.templates} onSearchTemplate={this.searchTemplates.bind(this)} ratesOptions={this.state.rates}
                      employers={this.state.employers} typesOptions={this.state.typesOptions} vendorsOptions={this.state.vendors}
                      onSavePlan={this.savePlanV3.bind(this)} updatePeriod={this.updatePeriod.bind(this)} />

          <OfferEditor planTypes={planTypes} plansList={this.state.plans} offers={this.state.offers}
                       onAddOffer={this.add.bind(this)} onSort={this.sort.bind(this)}
                       onRemoveOffer={this.remove.bind(this)} onUpdateOffer={this.updateOffer.bind(this)}/>
      </div>
    );
  }
}

PlanPeriodDetails.propTypes = {
  params: React.PropTypes.object
};

module.exports = PlanPeriodDetails;
