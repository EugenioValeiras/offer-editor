import React from 'react';
import BigOverLay from '../../lib/BigOverLay';

import MonthSelect from '../utils/MonthSelect.jsx';
import YearSelect from '../utils/YearSelect.jsx';

import {monthYearToDateMoment} from '../utils/Dates.jsx';

class AddPlanModal extends React.Component{
  constructor(props) {
    super(props);
    this.state = {plan: _.clone(this.props.plan), from: {month: null, year: null},
      to: {month: null, year: null}, errors: []};
  }

  componentWillMount() {
    let fromMoment = moment(this.props.plan.from);
    let toMoment = moment(this.props.plan.to);

    this.setState({from: {month: fromMoment.format('M'), year:fromMoment.format('YYYY')},
                    to: {month: toMoment.format('M'), year:toMoment.format('YYYY')}});
  }

  componentWillReceiveProps(nextProps) {
    let fromMoment = moment(nextProps.plan.from);
    let toMoment = moment(nextProps.plan.to);

    this.setState({plan: _.clone(nextProps.plan), errors: [],
      from: {month: fromMoment.format('M'), year:fromMoment.format('YYYY')},
      to: {month: toMoment.format('M'), year:toMoment.format('YYYY')}});
  }

  groupNoChanged(e) {
    let plan = this.state.plan;
    plan.groupNo = e.target.value;
    this.setState({plan: plan});
  }

  deductionTypeChange(e) {
    let plan = this.state.plan;
    plan.deductionType = e.currentTarget.value;
    this.setState({plan: plan});
  }

  groupStatusChange(e) {
    let plan = this.state.plan;
    plan.groupStatus = e.currentTarget.value;
    this.setState({plan: plan});
  }

  save(e) {
    e.stopPropagation();
    e.preventDefault();

    if (this.validate()) {
      let dateFrom = monthYearToDateMoment(this.state.from.month, this.state.from.year);
      this.state.plan.from = dateFrom ? dateFrom.format() : '';

      let dateTo = monthYearToDateMoment(this.state.to.month, this.state.to.year);
      let endDateTo = dateTo ? dateTo.endOf('month') : null;

      this.state.plan.to = endDateTo ? endDateTo.format() : '';
      this.props.onSave(this.state.plan);
      this.props.onHide();
    }
  }

  validate() {
    let valid = true;
    let errors = [];
    if (!this.state.plan.name) {
      valid = false;
      errors.push('name');
    }

    if (!this.state.from.month) {
      valid = false;
      errors.push('fromMonth');
    }

    if (!this.state.from.year) {
      valid = false;
      errors.push('fromYear');
    }

    if (!this.state.to.month) {
      valid = false;
      errors.push('toMonth');
    }

    if (!this.state.to.year) {
      valid = false;
      errors.push('toYear');
    }

    this.setState({errors: errors});
    return valid;
  }

  detailsChange(e) {
    let plan = this.state.plan;
    plan.details = e.currentTarget.value;
    this.setState({plan: plan});
  }

  fromDateChange(newValue) {
    let from = this.state.from;
    from.month = newValue;
    this.setState({from: from});
  }

  fromYearChange(newValue) {
    let from = this.state.from;
    from.year = newValue;
    this.setState({from: from});
  }

  toDateChange(newValue) {
    let to = this.state.to;
    to.month = newValue;
    this.setState({to: to});
  }

  toYearChange(newValue) {
    let to = this.state.to;
    to.year = newValue;
    this.setState({to: to});
  }

  rateChange(e) {
    let plan = this.state.plan;
    plan.rate = e.currentTarget.value;
    this.setState({plan: plan});
  }

  _ratesOptions() {
    return this.props.ratesOptions.map(function(type, index) {
      return (<option key={index} value={type.name}>{type.name}</option>);
    }, this);
  }

  render() {
    return (
      <BigOverLay header="Add Plan" show={this.props.show} onHide={this.props.onHide}>
        <form className="row add-plan-form">
          <div className="col-md-12">
            <div className="col-md-3 text-center">
              <img src={this.state.plan.imageUrl ? this.state.plan.imageUrl : 'https://placeholdit.imgix.net/~text?txtsize=20&txt=Logo&w=200&h=100'} width="200" height="100" /> <br />
              <h3>{this.state.plan.benefit_type}</h3>
            </div>
            <div className="col-md-9">
                <div className="row">
                  <div className={_.contains(this.state.errors, 'name') ? 'col-md-12 has-error':'col-md-12'}>
                    <label>{this.state.plan.name }</label>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-6">
                    <span>Coverage Start date on first day of</span> <br />
                    <div className={_.contains(this.state.errors, 'fromMonth') ? 'has-error' : null}>
                      <MonthSelect onChange={this.fromDateChange.bind(this)} value={this.state.from.month} /> <br />
                    </div>
                    <div className={_.contains(this.state.errors, 'fromYear') ? 'has-error' : null}>
                      <YearSelect onChange={this.fromYearChange.bind(this)} value={this.state.from.year} />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <span>Ends on last day of</span> <br />
                    <div className={_.contains(this.state.errors, 'toMonth') ? 'has-error' : null}>
                      <MonthSelect onChange={this.toDateChange.bind(this)} value={this.state.to.month} /> <br />
                    </div>
                    <div className={_.contains(this.state.errors, 'toYear') ? 'has-error' : null}>
                      <YearSelect onChange={this.toYearChange.bind(this)} value={this.state.to.year} />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-6">
                    <span>Deduction type</span> <br />
                    <label className="radio-inline">
                      <input type="radio" name="deductionType" value="Pre-Tax"
                             onChange={this.deductionTypeChange.bind(this)}
                             checked={this.state.plan.deductionType == 'Pre-Tax'} /> Pre-Tax
                    </label>&nbsp;&nbsp;
                    <label className="radio-inline">
                      <input type="radio" name="deductionType" value="Post-Tax"
                             onChange={this.deductionTypeChange.bind(this)}
                             checked={this.state.plan.deductionType == 'Post-Tax'}/> Post-Tax
                    </label>
                  </div>
                  <div className="col-md-6">
                    <button onClick={this.save.bind(this)} className="btn btn-success btn-green">Save</button>
                  </div>
                </div>
            </div>
          </div>
          <div className="col-md-12">
            <div className="col-md-3">
              <span>Rates</span> <br />
              <select className="form-control"
                      onChange={this.rateChange.bind(this)}
                      value={this.state.plan.rate ? this.state.plan.rate: ''} > >
                <option value="">Select...</option>
                {this._ratesOptions()}
              </select>
            </div>
            <div className="col-md-9">
              <div className="row">
                <div className="col-md-6">
                  <span>Group Status</span> <br />
                  <label className="radio-inline">
                    <input type="radio" name="groupStatus" value="new"
                           onChange={this.groupStatusChange.bind(this)}
                           checked={this.state.plan.groupStatus == 'new'} /> New
                  </label>&nbsp;&nbsp;
                  <label className="radio-inline">
                    <input type="radio" name="groupStatus" value="existing"
                           onChange={this.groupStatusChange.bind(this)}
                           checked={this.state.plan.groupStatus == 'existing'}/> existing
                  </label>
                </div>
                <div className="col-md-6 group-no">
                  <span>Group No:</span> <br />
                  <input className="editable-click" value={this.state.plan.groupNo || ''} onChange={this.groupNoChanged.bind(this)}   />
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-12">
            <span>Plan Details</span> <br />
            <textarea onChange={this.detailsChange.bind(this)} rows="20" cols="115" value={this.props.plan.details} />
          </div>
        </form>
      </BigOverLay>
    );
  }
}

AddPlanModal.propTypes = {
  show: React.PropTypes.bool,
  onHide: React.PropTypes.func,
  onSave: React.PropTypes.func,
  plan: React.PropTypes.object,
  ratesOptions: React.PropTypes.array
};

module.exports = AddPlanModal;
