import React from 'react';
import BigOverLay from '../../lib/BigOverLay';

class PlanFromLibraryModal extends React.Component{
  constructor(props) {
    super(props);
    this.state = {searchTerms:{}};
  }

  componentWillReceiveProps() {
    this.setState({searchTerms:{}});
  }

  searchLibrary() {
    this.props.onSearch(this.state.searchTerms);
  }

  searchTypeChange(e) {
    let searchTerms = this.state.searchTerms;
    searchTerms.type = e.currentTarget.value;
    this.setState({searchTerms: searchTerms});
  }

  searchVendorChange(e) {
    let searchTerms = this.state.searchTerms;
    searchTerms.vendor = e.currentTarget.value;
    this.setState({searchTerms: searchTerms});
  }

  _templates() {
    return this.props.templates.map(function(template, index) {
      return (
        <li key={'itemkey'+index} className="col-md-12 plan-item" >
          <div className="img-container">
            <img src={template.imageUrl ? template.imageUrl : 'https://placeholdit.imgix.net/~text?txtsize=15&txt=Logo&w=100&h=50'} width="100" height="50" />
          </div>
          <div className="name-container">
            <span>{template.name}</span><br />
            <span>{template.benefit_type}</span>
          </div>
          <div className="coverage-container"></div>
          <div className="actions-container">
            <button className="btn btn-link" type="button" onClick={this.props.onShowAddPlan.bind(this, template)}>
              <span className="glyphicon glyphicon-chevron-right" aria-hidden="true" />
            </button>
          </div>
        </li>
      );
    }, this);
  }

  _templatesTypesOptions() {
    return this.props.typesOptions.map(function(type, index) {
      return (<option key={index} value={type.name}>{type.name}</option>);
    }, this);
  }

  _vendorsOptions() {
    return this.props.vendorsOptions.map(function(type, index) {
      return (<option key={index} value={type.name}>{type.name}</option>);
    }, this);
  }


  render() {
    return (
      <BigOverLay header="Add Plan From Library" show={this.props.show} onHide={this.props.onHide}>
        <form className="row add-from-library-form">
          <div className="col-md-12">
            <span>Search for a plan template matching these criteria</span>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label>Benefit type</label>
              <select className="form-control"
                      onChange={this.searchTypeChange.bind(this)}
                      value={this.state.searchTerms.type ? this.state.searchTerms.type : '' }>
                <option value="">Select...</option>
                {this._templatesTypesOptions()}
              </select>
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label>Vendor</label>
              <select className="form-control"
                      onChange={this.searchVendorChange.bind(this)}
                      value={this.state.searchTerms.vendor ? this.state.searchTerms.vendor: ''} >
                <option value="">Select...</option>
                {this._vendorsOptions()}
              </select>
            </div>
          </div>
          <div className="col-md-4">
            <button className="btn btn-default" type="button" onClick={this.searchLibrary.bind(this)}>Search</button>
          </div>
        </form>
        <div className="row plans-from-library-list">
          <div className="col-md-12 title">
            <span>Search results</span>
          </div>

          <div className="plans-list">
            <ul className="items-list">
              {this.props.templates.length > 0 ? this._templates() : null}
            </ul>
          </div>
        </div>
      </BigOverLay>
    );
  }
}

PlanFromLibraryModal.propTypes = {
  show: React.PropTypes.bool,
  onHide: React.PropTypes.func,
  onShowAddPlan: React.PropTypes.func,
  typesOptions: React.PropTypes.array,
  vendorsOptions: React.PropTypes.array,
  templates: React.PropTypes.array,
  onSearch: React.PropTypes.func
};

module.exports = PlanFromLibraryModal;
