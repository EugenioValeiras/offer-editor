import React from 'react';
import { Link } from 'react-router';

import Table from 'react-bootstrap/lib/Table';
import Button from 'react-bootstrap/lib/Button';

import OfferStore from '../stores/OfferStore';
import OffersActions from '../actions/OffersActions';
import PlanModal from './PlanModal.jsx';

import {formatDatesPeriod} from '../utils/Dates.jsx';

class PlansPeriodList extends React.Component{
  constructor(props) {
    super(props);
    this.onStoreChange = this.onStoreChange.bind(this);
    this.state = _.extend(OfferStore.getState(), {modalOpen: false});
  }

  onStoreChange(state) {
    if (!state.loading) {
        this.unBlockUI();
    }
    this.setState(state);
  }

  add(period) {
    //Default enrollment window to period dates
    period.enrollmentFrom = period.from;
    period.enrollmentTo = period.to;
    OffersActions.addPeriod(this.state.periodsList, period);
  }

  openModal() {
    this.setState({modalOpen: true});
  }

  hideModal() {
    this.setState({modalOpen: false});
  }

  componentDidMount() {
    OfferStore.listen(this.onStoreChange);
    this.blockUI();
    OffersActions.fetchPeriods();
    OffersActions.fetchEmployers();
  }

  componentWillUnmount() {
    OfferStore.unlisten(this.onStoreChange);
  }

  _periods() {
    return this.state.periodsList.map(function(period, index) {
      let fromMoment = moment(period.from);
      let toMoment = moment(period.to);
      return (
      <tr key={index}>
        <td>{period.name}</td>
        <td>{fromMoment.format('DD-MMM-YYYY')}</td>
        <td>{toMoment.format('DD-MMM-YYYY')}</td>
        <td>{period.status}</td>
        <td>
          <Link to={`/dashboard/${period.objectId}`}><span className="glyphicon glyphicon-chevron-right" aria-hidden="true" /></Link>
        </td>
      </tr>
      );
    }, this);
  }

  blockUI() {
    $(this.refs.mainDiv).block({overlayCSS: {opacity: 0.3}, message: null });
  }

  unBlockUI() {
    $(this.refs.mainDiv).unblock();
  }

  render() {
    return (
      <div ref="mainDiv" className="container">
        <PlanModal show={this.state.modalOpen} onHide={this.hideModal.bind(this)} employers={this.state.employers}
                   planPeriod={null} updatePeriod={this.add.bind(this)} />
        <div>
          <Button bsStyle="success" className="btn-green" onClick={this.openModal.bind(this)} >+ Add Plan Period</Button>
        </div>
        <div className="plans-periods">
          <Table>
            <thead>
              <tr>
                <th>Plan Period Name</th>
                <th>Start On</th>
                <th>Ends On</th>
                <th>Status</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this._periods()}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

PlansPeriodList.propTypes = {

};

module.exports = PlansPeriodList;
