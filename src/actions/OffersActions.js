import alt from '../alt';
import OfferSource from '../sources/OfferSource';

class OffersActions {

  constructor(){
    this.timeOut = null;
  }

  _getList(periodId) {
    return () => {
      OfferSource.fetch(periodId)
        .done((data) => {
          this.updateOffers(data.results);
        })
    }
  }

  callUpload(periodId) {
    return () => {
      OfferSource.callUpload(periodId);
    }
  }

  fetchVendors() {
    return () => {
      OfferSource.fetchVendors()
        .done((data) => {
          this.updateVendors(data.results);
        })
    }
  }

  fetchRates() {
    return () => {
      OfferSource.fetchRates()
        .done((data) => {
          this.updateRates(data.results);
        })
    }
  }

  updateRates(rates) {
    return rates;
  }

  fetchPlans() {
    return () => {
      OfferSource.fetchPlans()
        .done((data) => {
          this.updatePlans(data.results);
        })
    }
  }

  fetchTypes() {
    return () => {
      OfferSource.fetchTypesOptions()
        .done((data) => {
          this.updateTypes(data.results);
        })
    }
  }

  fetchEmployers() {
    return () => {
      OfferSource.fetchEmployers()
        .done((data) => {
          this.updateEmployers(data.results);
        })
    }
  }

  fetchPlansV2(periodId) {
    return () => {
      OfferSource.fetchPlansV2(periodId)
        .done((data) => {
          this.updatePlansV2(data.results);
        })
    }
  }

  fetchTemplates(query) {
    return () => {
      OfferSource.fetchTemplates(query)
        .done((data) => {
          return this.updateTemplates(data.results);
        })
    };
  }

  updateTemplates(templates) {
      return templates;
  }

  editPlanV2(plan) {
    return () => {
      OfferSource.updatePlanV2(plan)
        .done((plan) => {
          return this.fetchPlansV2(plan.periodId);
        })
    };
  }

  addPlanV2(plan) {
    return () => {
      OfferSource.addPlanV2(plan)
        .done(() => {
          return this.fetchPlansV2(plan.periodId);
        })
    };
  }

  fetchOffers(periodId) {
    return (dispatch) => {
      // we dispatch an event here so we can have "loading" state.
      dispatch();
      this._getList(periodId);
    }
  }

  _getPeriodList() {
    return () => {
      OfferSource.fetchPeriodList()
        .done((data) => {
          this.updatePeriods(data.results);
        })
    }
  }

  fetchPeriods() {
    return (dispatch) => {
      // we dispatch an event here so we can have "loading" state.
      dispatch();
      this._getPeriodList();
    }
  }

  updatePeriods(periods) {
    return periods;
  }

  syncOrder(offers, offerItem, newOrder) {
    var modelWithNewOrder = _.findWhere(offers, {order: newOrder}); //Find the offer to update the position
    modelWithNewOrder.order = offerItem.order;
    offerItem.order = newOrder;

    OfferSource.update(modelWithNewOrder);
    OfferSource.update(offerItem);

    return this.updateOffers(offers);
  }

  updatePlans(plans) {
    plans.map(function(plan) {
        plan.id = plan.objectId;
        plan.text = plan.name;
    });
    return plans;
  }

  updatePlansV2(plans) {
    return plans;
  }

  updateEmployers(employers) {
    return employers;
  }

  updateTypes(options) {
    return options;
  }

  updateVendors(vendors) {
    return vendors;
  }

  updateOffers(offers) {
    //Sort offers by order and default
    return offers.sort((a, b) => {
      if (a.default) { //Keep the default offer at the bottom
        return 1;
      } else if (b.default) {
        return -1;
      } else if (a.order > b.order) {
        return 1;
      } else {
        return -1;
      }
    });
  }

  addNew(offers, newOffer) {
    //Reorder collection
    var order = 1;
    offers.forEach(function(offer) {
      if (!offer.default) {
        offer.order = ++order;
        OfferSource.update(offer);
      }
    });

    offers.push(newOffer);
    return this.updateOffers(offers);
  }

  addOffer(offers, offer) {
    return () => {
        OfferSource.add(offer)
          .done((data) => {
            offer.objectId = data.objectId;
            return this.addNew(offers, offer);
          })
    };
  }

  addPeriod(periods, period) {
    return () => {
      OfferSource.addPeriod(period)
        .done((data) => {
          period.objectId = data.objectId;
          periods.push(period);
          //Create default Offer
          OfferSource.add({name: null, default: true, order: 1, periodId: period.objectId});
          return this.updatePeriods(periods);
        })
    };
  }

  _remove(offers, toRemove) {
    offers = _.without(offers, toRemove);
    //Reorder collection
    var order = 1;
    offers.forEach(function(offer) {
      if (!offer.default) {
        offer.order = order++;
        OfferSource.update(offer);
      }
    });

    return this.updateOffers(offers);
  }

  removeOffer(offers, offer) {
    return () => {
      OfferSource.remove(offer)
        .done(() => {
          return this._remove(offers, offer);
        })
    };
  }

  updateOffer(offer) {
    return () => {
      OfferSource.update(offer);
    };
  }

  changePeriod(period) {
    return () => {
      OfferSource.updatePeriod(period).done((data) => {
        data.objectId = period.objectId;
        return this.updatePeriod(data);
      });
    };
  }

  updatePeriod(period) {
    return period;
  }

  fetchPeriod(periodId) {
    return () => {
      OfferSource.fetchPeriod(periodId)
        .done((data) => {
          return this.updatePeriod(data);
        })
    }
  }

  getCounts(planPeriodId) {
    return () => {
      OfferSource.getCounts(planPeriodId)
        .done((data) => {
          return this.updateCounts(data);
        })
    }
  }

  updateCounts(counts) {
    return counts;
  }

  fetchPeople(query, planPeriodId) {
    return (dispatch) => {
      // we dispatch an event here so we can have "loading" state.
      dispatch();
      this._getPeopleList(query, planPeriodId);
    }
  }

  _getPeopleList(query, planPeriodId) {
    return () => {
      OfferSource.fetchPeople(query, planPeriodId)
        .done((data) => {
          return this.updatePeople(data);
        })
    }
  }

  updatePeople(people) {
    return people;
  }

  changePerson(person) {
    return () => {
      OfferSource.updatePerson(person).done((data) => {
        clearTimeout(this.timeOut);
        this.timeOut = setTimeout(this.getCounts, 1000); //Add delay
        return this.updatePerson(data);
      });
    };
  }

  updatePerson(person) {
    return person;
  }

}

module.exports = alt.createActions(OffersActions);
