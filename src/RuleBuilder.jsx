import React from 'react';
import { PeopleFilter, Builder, Preview, Buttons } from '../lib/ReactQueryBuilder';
import config from './config';

class RuleBuilder extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <PeopleFilter {...config} show={this.props.show} onHide={this.props.onHide}>
            {(props) => (
                <div>
                    <div className="query-builder">
                        <Builder {...props} />
                    </div>
                    <Buttons {...props} onSave={this.props.onSave}/>
                </div>
            )}
        </PeopleFilter>
    );
  }
}

RuleBuilder.propTypes = {
  show: React.PropTypes.bool,
  onHide: React.PropTypes.func,
  onSave: React.PropTypes.func
};

module.exports = RuleBuilder;
