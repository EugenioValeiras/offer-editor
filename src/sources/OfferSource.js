
var OfferSource =  {
  setHeaders() {
    //Parse API configuration
    $.ajaxSetup({
      headers: {
        'X-Parse-REST-API-Key': 'WjUEOpQFvCcYy2ix7Z4x3FZkE56S6cgWeThCquq3',
        'X-Parse-Application-Id': 'OTQMdHSEZY8UdVT8vVUFg56WQquDshHB7oR4M548'
      }
    });
  },

  callUpload(periodId) {
    return $.get('https://api.parse.com/1/classes/UploadCensus/'+periodId);
  },

  fetch (periodId) {
    this.setHeaders();
    return $.get( 'https://api.parse.com/1/classes/Offer/', { where: {'periodId': periodId} });
  },

  fetchTemplates(query) {
    let where = {};
    if (!_.isEmpty(query)) {
        if (query.type) {
          where['benefit_type'] = query.type;
        }

        if (query.vendor) {
          where['vendor'] = query.vendor;
        }
    }
    this.setHeaders();
    return $.get( 'https://api.parse.com/1/classes/Template/', { where: where });
  },

  remove(offer) {
    this.setHeaders();
    return $.ajax({method:'DELETE', url: 'https://api.parse.com/1/classes/Offer/'+offer.objectId});
  },

  add(offer) {
    this.setHeaders();
    return $.post('https://api.parse.com/1/classes/Offer/', JSON.stringify(offer));
  },

  addPeriod(period) {
    this.setHeaders();
    return $.post('https://api.parse.com/1/classes/PlanPeriod/', JSON.stringify(period));
  },

  update(offer) {
    this.setHeaders();
    return $.ajax({method:'POST', url: 'https://api.parse.com/1/classes/Offer/'+offer.objectId, data: JSON.stringify(offer)});
  },

  fetchPlans () {
    this.setHeaders();
    return $.get( 'https://api.parse.com/1/classes/Plan/');
  },

  fetchEmployers() {
    this.setHeaders();
    return $.get( 'https://api.parse.com/1/classes/employers/');
  },

  fetchPlansV2 (periodId) {
    this.setHeaders();
    return $.get( 'https://api.parse.com/1/classes/PlanV2/', { where: {'periodId': periodId} });
  },

  addPlanV2(plan) {
    this.setHeaders();
    return $.post( 'https://api.parse.com/1/classes/PlanV2/', JSON.stringify(plan));
  },

  updatePlanV2(plan) {
    this.setHeaders();
    return $.ajax({method:'POST', url: 'https://api.parse.com/1/classes/PlanV2/'+plan.objectId, data: JSON.stringify(plan)});
  },

  fetchPeriod(periodId) {
    this.setHeaders();
    return $.get( 'https://api.parse.com/1/classes/PlanPeriod/'+periodId);
  },

  fetchPeriodList() {
    this.setHeaders();
    return $.get( 'https://api.parse.com/1/classes/PlanPeriod/');
  },

  updatePeriod(period) {
    this.setHeaders();
    return $.ajax({method:'POST', url: 'https://api.parse.com/1/classes/PlanPeriod/'+period.objectId, data: JSON.stringify(period)});
  },

  fetchTypesOptions() {
    this.setHeaders();
    return $.get('https://api.parse.com/1/classes/BenefitTypes/');
  },

  fetchVendors() {
    this.setHeaders();
    return $.get('https://api.parse.com/1/classes/vendor/');
  },

  fetchRates() {
    this.setHeaders();
    return $.get('https://api.parse.com/1/classes/rate/');
  },

  fetchPeople(query, planPeriodId) {
    console.log('fetch people');
    console.log(planPeriodId);
    //TODO: planPeriodId
    this.setHeaders();
    let where = {};

    if (query && query.byName) {
      where[query.byName.field] = {'$regex': query.byName.value, '$options':'i'};
    }

    if (query && query.byBoolean) {
      where[query.byBoolean.field] = query.byBoolean.value;
    }

    return $.get('https://api.parse.com/1/classes/People/', { where: JSON.stringify(where), limit: query.limit, skip: query.offset, count:1});
  },

  getCounts(planPeriodId) {
    console.log('source get counts');
    console.log(planPeriodId);
    //TODO:
    ///PeopleList/PlanPeriodId - GET
    return $.get('');
  },

  updatePerson(person) {
    this.setHeaders();
    return $.ajax({method:'POST', url: 'https://api.parse.com/1/classes/People/'+person.objectId, data: JSON.stringify(person)});
  }

};

module.exports = OfferSource;
