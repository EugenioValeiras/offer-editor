import React from 'react';

class PersonItem extends React.Component{
  constructor(props) {
    super(props);
    this.state = {name: null};
  }

  componentWillMount() {
    this.state = {name: null};
  }

  componentWillReceiveProps() {
    this.state = {name: null};
  }

  toggleProperty(property) {
    let person = this.props.person;
    person[property] = !person[property];
    this.props.onUpdatePerson(person);
  }

  onRowClick(e) {
    if (e.target.tagName != 'SPAN') {
      this.props.onToggleSelect(this.props.person);
    }
  }

  selectedClass(columnId) {
    return this.props.selectedColumn === columnId ? 'selected': null;
  }

  renderIcon(booleanProp, onlyTick) {
    return (<span className={booleanProp ? 'glyphicon glyphicon-ok' : onlyTick ? null : 'glyphicon glyphicon-remove'} />)
  }

  render() {
    let person = this.props.person;
    return (
      <tr className={this.props.selected ? 'selected-item' : null} onClick={this.onRowClick.bind(this)}>
        <td className="person-data">
          <div>
            <img src={person.imageURL ? person.imageURL : 'https://placeholdit.imgix.net/~text?txtsize=33&w=50&h=50'}
                 height="50" width="50" className="img-circle" />	&nbsp;
            <span className="editable-click">{person.name}</span>
          </div>
        </td>
        <td></td>
        <td className={this.selectedClass('eligible')} onClick={this.toggleProperty.bind(this,'eligible')}>
          {this.renderIcon(person.Eligible)}
        </td>
        <td className={this.selectedClass('excluded')} onClick={this.toggleProperty.bind(this,'excluded')}>
          {this.renderIcon(person.Excluded, true)}
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    );
  }
}

PersonItem.propTypes = {
  person: React.PropTypes.object,
  selected: React.PropTypes.bool,
  onToggleSelect: React.PropTypes.func,
  onUpdatePerson: React.PropTypes.func,
  selectedColumn: React.PropTypes.string
};

module.exports = PersonItem;
