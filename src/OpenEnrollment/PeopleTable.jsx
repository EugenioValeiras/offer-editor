import React from 'react';
import PersonItem from './PersonItem.jsx';

class ActionsBar extends React.Component{
  constructor(props) {
    super(props);
    this.timeOut = null;
    this.state = {selectedColumn: null, booleanFilter: null, nameFilter: null}
  }

  selectColumn(columnId) {
    if (this.state.selectedColumn !== columnId) {
      this.setState({selectedColumn: columnId, booleanFilter: {field: columnId, value: true}}, this.searchPeople);
    } else {
      this.setState({selectedColumn: null, booleanFilter: null}, this.searchPeople);
    }
  }

  filterChange(e) {
    clearTimeout(this.timeOut);
    let searchValue = e.target.value;
    this.timeOut = setTimeout(() => {
      this.setState({nameFilter: searchValue ? {field: 'name', value: searchValue} : null}, this.searchPeople);
    }, 300); //Add delay
  }

  searchPeople() {
    //nameFilter
    this.props.onSearch({byName: this.state.nameFilter, byBoolean: this.state.booleanFilter})
  }

  people() {
    return this.props.list.map(function(person, index) {
      return (<PersonItem key={index} person={person} selectedColumn={this.state.selectedColumn} selected={!!_.find(this.props.selectedItems, person)}
                          onUpdatePerson={this.props.onUpdatePerson} onToggleSelect={this.props.onToggleSelect} />);
    }, this);
  }

  selectedClass(columnId) {
    return this.state.selectedColumn === columnId ? 'selected': null;
  }

  render() {
    return (
      <table className="table enrollment-table">
        <thead>
        <tr className="first-header">
          <td>
            <button className="btn btn-link">Name</button>/Assigned to
          </td>
          <td>Total Census</td>
          <td>Eligible</td>
          <td>Excluded</td>
          <td>SS Off</td>
          <td>SS Pending</td>
          <td>SS On</td>
          <td>Step 1 (UPI)</td>
          <td>Step 2 (Elect)</td>
          <td>Step 3 (Forms)</td>
          <td>Done</td>
          <td>Waived</td>
          <td>Have Forms</td>
          <td>Forms Ready</td>
          <td>Forms Posted</td>
          <td></td>
        </tr>
        <tr>
          <th>
            <div className="search-box form-group">
              <div className="input-group">
                <div className="input-group-addon">
                  <span className="glyphicon glyphicon-search" />
                </div>
                <input type="text" className="form-control" onChange={this.filterChange.bind(this)} />
              </div>
            </div>
          </th>
          <th className={this.selectedClass('TotalCensus')} onClick={this.selectColumn.bind(this, 'TotalCensus')}>
            {this.props.counts.TotalCensus}
          </th>
          <th className={this.selectedClass('eligible')} onClick={this.selectColumn.bind(this, 'eligible')}>
            {this.props.counts.Eligible}
          </th>
          <th className={this.selectedClass('excluded')} onClick={this.selectColumn.bind(this, 'excluded')}>
            {this.props.counts.Excluded}
          </th>
          <th className={this.selectedClass('SSOff')} onClick={this.selectColumn.bind(this, 'SSOff')}>
            {this.props.counts.SSOff}
          </th>
          <th className={this.selectedClass('SSPending')} onClick={this.selectColumn.bind(this, 'SSPending')}>
            {this.props.counts.SSPending}
          </th>
          <th className={this.selectedClass('SSOn')} onClick={this.selectColumn.bind(this, 'SSOn')}>
            {this.props.counts.SSOn}
          </th>
          <th className={this.selectedClass('StatusUPI')} onClick={this.selectColumn.bind(this, 'StatusUPI')}>
            {this.props.counts.StatusUPI}
          </th>
          <th className={this.selectedClass('StatusElection')} onClick={this.selectColumn.bind(this, 'StatusElection')}>
            {this.props.counts.StatusElection}
          </th>
          <th className={this.selectedClass('StatusEnrollment')} onClick={this.selectColumn.bind(this, 'StatusEnrollment')}>
            {this.props.counts.StatusEnrollment}
          </th>
          <th className={this.selectedClass('StatusDone')} onClick={this.selectColumn.bind(this, 'StatusDone')}>
            {this.props.counts.StatusDone}
          </th>
          <th className={this.selectedClass('Waived')} onClick={this.selectColumn.bind(this, 'Waived')}>
            {this.props.counts.Waived}
          </th>
          <th className={this.selectedClass('HaveForms')} onClick={this.selectColumn.bind(this, 'HaveForms')}>
            {this.props.counts.HaveForms}
          </th>
          <th className={this.selectedClass('FormsReady')} onClick={this.selectColumn.bind(this, 'FormsReady')}>
            {this.props.counts.FormsReady}
          </th>
          <th className={this.selectedClass('FormsPosted')} onClick={this.selectColumn.bind(this, 'FormsPosted')}>
            {this.props.counts.FormsPosted}
          </th>
          <th></th>
        </tr>
        </thead>
        <tbody>
          {this.people()}
        </tbody>
      </table>
    );
  }
}

ActionsBar.propTypes = {
  list: React.PropTypes.array,
  selectedItems: React.PropTypes.array,
  onToggleSelect: React.PropTypes.func,
  onSearch: React.PropTypes.func,
  onUpdatePerson: React.PropTypes.func,
  counts: React.PropTypes.object
};

module.exports = ActionsBar;
